# Django Postgres Docker Container

Example of how to create a Docker container for Django and Postgres

#### Create Django Project

``docker-compose run web django-admin.py startproject mysite.``

#### Run Django Project

``docker-compose run web dpython3 manage.py runserver 0.0.0.0:8000``

