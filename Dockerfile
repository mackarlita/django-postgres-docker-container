# Use an official Python runtime as a parent image
FROM python:3.7.0

# Define environment variables
ENV NAME django-test
ENV PYTHONUNBUFFERED 1

# Set the working directory to /app
RUN mkdir /app
WORKDIR /app

# Install any needed packages specified in requirements.txt
ADD requirements.txt /app/
RUN pip install -r requirements.txt

# Copy the current directory contents into the container at /app
ADD . /app

# Make port 80 available to the world outside this container
EXPOSE 80